#!/usr/bin/bash
#
# 2020 May
# This script reads a folder containing results after runnin the
# SeqCOVID-Illumina-pipeline and creates:
# Metadata files for submissions GISAID, MICROREACT, ENA...
# Concatenated multifasa files containing consensus that passed QC
#                                         with appropriate headers
# Reports for the conseortium
usage () {

	echo "$(basename $0)"
	echo "Usage:"
	echo "	-h	Display this help message"
	echo "	-d  DIRECTOY to collect data from"

exit 1
}
################################################################################
#======================== Create Metadata
################################################################################
# Create metadata tables ready for GISAID, MICROREACT and ENA submission
create_metadata () {
echo "--- Creating metadata tables"
python3.4 ${SCRIPT_DIR}/../utils/COVID-analysis-metadata.py  \
							-m ${MTABLE_REAL} --run-id ${RUNID} \
							--run-directory ${OUTFOLDER_REAL} \
							-o ${OUTFOLDER_REAL}/metadata
}
################################################################################
#======================== Concatenated Multifasta
################################################################################
# Create a multifasta with genomes that pass QC threshold of 90% coverage and
#  ready for manual inspection for GISAID upload and
# lineae assinment with pangolin
multifasta_consensus () {
  echo "--- Creating HQ multifasta"
	# Read which genomes pass QC 90% from summary
	tail -n +2 reports/QC_summary.${RUNID}.txt |
	   awk -v FS=';' '$5 >= 0.9' | cut -f 1 -d ';' | while read COV
		 do
			 sed -e "s/Consensus_//" consensus/${COV}.fa \
		 	    -e "s/_threshold_${MINFREQ_CONS}_quality_${MINQUAL_CONS}//" > \
		 				 consensus/${COV}.tmp
		 			mv consensus/${COV}.tmp consensus/${COV}.fa

		  cat consensus/${COV}.fa
		done > consensus/consensus.${RUNID}.fasta
	# Create a consensus with GISAID headers read from metadata
  echo "--- Creating GISAID multifasta with SARS-Cov-2 reference genome"
	cat /data2/COVID19/reference_data/Sars-Cov-2_REFERENCE.fasta > consensus/GISAID_consensus.${RUNID}.fasta
  tail -n +2 metadata/GISAID_${RUNID}_metadata.csv |
	cut -f3,26 -d'@' | sed 's/@/ /' | while read GISAID_ID COV_ID
	  do
		  cat consensus/${COV_ID}.fa | sed "s|${COV_ID}|${GISAID_ID}|"
		done >> consensus/GISAID_consensus.${RUNID}.fasta
}
################################################################################
#            	    	 **********************************
# 															MAIN SCRIPT
#               	   **********************************
################################################################################
while getopts ":hd:" option
 do
  case "${option}" in
    h)
      usage
      exit 1;;
    d) RUNDIR=${OPTARG};;
    \?)
			usage
			exit 1;;
		:)
			echo "Invalid option: $OPTARG requires an argument" >&2;;
	esac
done
shift "$(($OPTIND -1))"

if [ -z ${RUNDIR} ]; then
  echo ""
  echo "No input directory provided"
  echo ""
  usage
  exit 1
fi
if [ ! -d ${RUNDIR} ]; then
  echo ""
  echo "Input directory does not exist"
  echo ""
  exit 1
else
  RUNDIR_REAL=$(readlink -f ${RUNDIR})
fi
# TRY to check if that directory contains folders that sould be present after
# analysis
if [ ! -d ${RUNDIR_REAL}/logfolder ]; then
  echo ""
  echo "The directory provided does not contain a logdfolder"
  echo "Sure you are providing the appropriate directory?"
  echo ""
  exit 1
fi
################################################################################
# Get relative paths and load RUN variables from logfile
################################################################################
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
# Get run variables from logfolder/RUN.RUNID.variables
source ${RUNDIR_REAL}/logfolder/RUN.*.variables
# MOVE TO RUNDIR_REAL DIRECTORY
cd ${RUNDIR_REAL}
################################################################################
#											 CREATE METADATA FILES
################################################################################
create_metadata

################################################################################
#											 CREATE MULTIFASTA CONSENSUS
################################################################################
# Create a multifasta consensus only with those consensus that meet QC
# requirements (>90% coverage). Also create a GISAID-ready consensus where
# previously generated metadata is used to change the header from
# COV0000X TO hCoV-19/Spain/Andalusia/COV0000X/2020
multifasta_consensus # CREATE CONSENSUS


exit 0
