#!/bin/bash

FAST5FOLDER=$1
RUN=$2
SAMPLES=$3
RUNFOLDER=${RUN}_$(date +"%y%m%d%H%M%S")

#=== get needed variables
source $NCOV_MAPPING_WD/variables.sh

echo $NCOV_MAPPING_WD
#=== Config viarable
echo "ARTIC_PRIMER_SCHEMA_PATH=$ARTIC_PRIMER_SCHEMA_PATH"
echo "ARTIC_PRIMER_SCHEMA_VERSION=$ARTIC_PRIMER_SCHEMA_VERSION"
echo "GUPPY_PATH=$GUPPY_PATH"
echo "GUPPY_IP_PORT=$GUPPY_IP_PORT"

#
# Defining working variables
WF=$PWD/${RUNFOLDER}
RESULTS=${WF}/analysis
FASTQPASS=${WF}/fastq_pass
MAPPING=${WF}/$mapping
CONSENSUS=${WF}/consensus
VARIANTS=${WF}/variants

echo "=== Working folders ====================="
echo "RUN               : ${RUN}"
echo "FAST5 folder      : ${FAST5FOLDER}"
echo "Working flder     : ${WF}"
echo "Results folder    : ${RESULTS}"
echo "FASTQ pass folder : ${FASTQPASS}" 
echo "consensus folder  : ${CONSENSUS}"
echo "variants folder   : ${VARIANTS}"
echo "Samples array     : ${SAMPLES}"

# Creating results folder
echo "=== Creating results folder:"
echo "mkdir -p ${RESULTS}"
mkdir -p ${CONSENSUS} ${VARIANTS} ${FASTQPASS} ${RESULTS}


# Running guppy for fast5 to fastq conversion
echo "=== GUPPY5 FAST5 to FASTQ"
echo "$GUPPYPATH/guppy_basecaller -c dna_r9.4.1_450bps_hac.cfg -i ${FAST5FOLDER} -s  ${FASTQPASS} -x auto -r --cpu_threads_per_caller 20 --num_callers 4 -q 4000"
# $GUPPY_PATH/guppy_basecaller -c dna_r9.4.1_450bps_hac.cfg -i ${FAST5FOLDER} -s ${FASTQPASS} --port $GUPPY_IP_PORT

echo "=== GUPPY Demultiplexing"

$GUPPY_PATH/guppy_barcoder --require_barcodes_both_ends -i ${FASTQPASS} -s ${RESULTS}/${RUN} --arrangements_files "barcode_arrs_nb12.cfg barcode_arrs_nb24.cfg" -t ${THREADS}

# Collecting fastq
echo "=== Collecting all fastq data:"
echo "artic gather --min-length 200  --max-length 800 --prefix ${RUN} --directory ${FASTQPASS}"
# artic gather --min-length 250  --max-length 900 --prefix ${RESULTS}/${RUN} --directory ${FASTQPASS}

echo "=== artic QC guppyplex"
artic guppyplex --min-length 250 --max-length 700 --directory output_directory/barcode03 --prefix run_name

exit 

#==============================================
#=========== Under Update    ==============
#==============================================

# Demultiplexing fastq
echo "=== Demultiplecing fastq data:"
echo "artic demultiplex --threads 40 ${RUN}_all.fastq"
artic demultiplex --threads 40 ${RESULTS}/${RUN}_all.fastq

# Mapping results

# Defining first array separation
IFS=","
read -ra SS <<< $SAMPLES

for SA in "${SS[@]}"
  do echo $SA

  IFS=":"
  read -ra SA2 <<< $SA
  BARCODE="${SA2[0]}"
  SAMPLE="${SA2[1]}"

  echo "=== Working on: BARCODE=${BARCODE} SAMPLE=${SAMPLE}"

  echo "=== Create sample ${SAMPLE} folder:"
  echo "mkdir ${RESULTS}/${SAMPLE}"
  mkdir -p ${RESULTS}/${SAMPLE}

  echo "=== Running artic minion pipeline"
  echo  "artic minion --medaka --normalise 200 --threads 40 --scheme-directory ${ARTIC_PRIMER_SCHEMA_PATH} --read-file $RESULTS/${RUN}_-${BARCODE}.fastq ${ARTIC_PRIMER_SCHEMA_VERSION} ${RESULTS}/${SAMPLE}/${SAMPLE}-${BARCODE}"
  artic minion --medaka --normalise 200 --threads 40 --scheme-directory ${ARTIC_PRIMER_SCHEMA_PATH} --read-file $RESULTS/${RUN}_-${BARCODE}.fastq ${ARTIC_PRIMER_SCHEMA_VERSION} ${RESULTS}/${SAMPLE}/${SAMPLE}-${BARCODE}

  echo "=== Renaming consensus:"
  echo "sed -i 's/^>.*/>'${SAMPLE}'/g' ${RESULTS}/${SAMPLE}/${SAMPLE}-${BARCODE}.consensus.fasta"
  sed -i 's/^>.*/>'${SAMPLE}'/g' ${RESULTS}/${SAMPLE}/${SAMPLE}-${BARCODE}.consensus.fasta

  echo "=== Copying consensus:"
  echo "cp ${RESULTS}/${SAMPLE}/${SAMPLE}-${BARCODE}.consensus.fasta ${CONSENSUS}"


  cp ${RESULTS}/${SAMPLE}/${SAMPLE}-${BARCODE}.consensus.fasta ${CONSENSUS}

  echo "cp ${RESULTS}/${SAMPLE}/${SAMPLE}-${BARCODE}.primertrimmed.medaka.vcf ${VARIANTS}"
  cp ${RESULTS}/${SAMPLE}/${SAMPLE}-${BARCODE}.primertrimmed.medaka.vcf ${VARIANTS}

done

echo "=== Concatenating consensus results:"
echo "cat ${CONSENSUS}/*.fasta > ${WF}/consensus.fasta"
cat ${CONSENSUS}/*.fasta > ${WF}/consensus.fasta

echo "=== Compressing foders:"
echo "tar cvzf ${RUNFOLDER}.tar.gz ${RUNFOLDER}/analysis/${RUN}_all.fastq  ${RUNFOLDER}/analysis/${RUN}_-NB??.fastq ${RUNFOLDER}/analysis/*/*.primertrimmed.sorted.bam* ${RUNFOLDER}/analysis/*/*.report.txt ${RUNFOLDER}/analysis/*/*.consensus.fasta ${RUNFOLDER}/analysis/*/*.primertrimmed.medaka.vcf ${RUNFOLDER}/consensus ${RUNFOLDER}/variants ${RUNFOLDER}/consensus.fasta"
tar cvzf ${RUNFOLDER}.tar.gz ${RUNFOLDER}/analysis/${RUN}_all.fastq  ${RUNFOLDER}/analysis/${RUN}_-NB??.fastq ${RUNFOLDER}/analysis/*/*.primertrimmed.sorted.bam* ${RUNFOLDER}/analysis/*/*.report.txt ${RUNFOLDER}/analysis/*/*.consensus.fasta ${RUNFOLDER}/analysis/*/*.primertrimmed.medaka.vcf ${RUNFOLDER}/consensus ${RUNFOLDER}/variants ${RUNFOLDER}/consensus.fasta



echo "=== CheckSum:"
echo "md5sum ${RUNFOLDER}.tar.gz > ${RUNFOLDER}.md5sum"
md5sum ${RUNFOLDER}.tar.gz > ${RUNFOLDER}.md5sum


echo "============ Done!! ==============="



