#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Utilities to load the DB credentials from the db.config file
Also create DB connections
'''
#---------------------------------------------------------------
__author__      = "Santiago Jiménez-Serrano"
__credits__     = ["Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import sys
import configparser
import mysql.connector


def load_credentials():
    '''
    Load the database credentials from the db.config file
    '''

    # Constant value to the credentials db.config file
    configfile = '/data2/COVID19/software/sars-cov2-mapping/utils/db.config'

    # Load & return the configuration
    config = configparser.ConfigParser()
    config.read(configfile)
    return config



def new_connection():
    '''
    Creates and returns a new connection to the SeqCOVID database
    '''

    # Get the credentials from the db.config file
    config = load_credentials()

    # Get the credentials
    user     = config.get('DB', 'user')
    password = config.get('DB', 'password')
    host     = config.get('DB', 'host')
    database = config.get('DB', 'database')

    # Login into the database
    try:
        cnx = mysql.connector.connect(
            user     = user,
            password = password,
            host     = host,
            database = database)
    except:
        sys.stderr.write(
            "ERROR: Can't connect to the DB: " + host + ", " + database + ", " + user + "\n")
        cnx = None

    # Return the mysql connection
    return cnx


def main():
    '''
    Test function => Loads and prints the DB credentials
    '''

    config = load_credentials()
    print("DB.user     = ", config.get('DB', 'user'))
    print("DB.password = ", config.get('DB', 'password'))
    print("DB.host     = ", config.get('DB', 'host'))
    print("DB.database = ", config.get('DB', 'database'))



if __name__ == "__main__":
    main()
