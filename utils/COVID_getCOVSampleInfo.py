#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Utilities to get the DATA saved in the hsCovid DB regarding a given COV sample
'''
#---------------------------------------------------------------
__author__      = "Santiago Jiménez-Serrano"
__credits__     = ["Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import sys
import argparse
import COVID_Utils             as cutils
import COVID_SpainMap_Utils    as sp_map_utils
import COVID_SQL_Queries       as dbqueries


def parse_args():
    '''
    Parse arguments given to script
    '''

    parser = argparse.ArgumentParser(description="Gets the DATA saved in the hsCovid DB regarding a given COV sample")
    parser.add_argument("-cov", "--cov", dest="cov", required=True)
    parser.add_argument("-silent", dest="silent", type=cutils.str2bool, nargs='?', const=True, default=False, help="Specify silent mode")

    args = parser.parse_args()

    return args



def parse_SQL_info(SQL_info, silent):
    '''
    Given the data returned by the SQL query, parse it, clean it
    and store into a dict
    '''

    # Initialize the dictionary
    COV_info = {}

    # Check the table status
    COV_info["RESULT"] = cutils.getSQLTableStatus(SQL_info)
    if COV_info["RESULT"] != "DATA":
        return COV_info

    # Get the record
    record = SQL_info[0]

    # Parse the main fields
    COV_info["s.cov_id"]                 = cutils.number2cov(record[1])
    COV_info["s.id"]                     = record[ 0]
    COV_info["s.gId"]                    = record[ 1]
    COV_info["s.original_seq_id"]        = record[ 2]
    COV_info["s.reception_date"]         = record[ 3]
    COV_info["s.hospital_date"]          = record[ 4]
    COV_info["s.status"]                 = record[ 5]
    COV_info["s.project_id"]             = record[ 6]
    COV_info["s.hospital_sample_number"] = record[ 7]
    COV_info["s.tube_id"]                = record[ 8]
    COV_info["s.sip"]                    = record[ 9]
    COV_info["s.sip_type"]               = record[10]
    COV_info["s.sip2"]                   = record[11]
    COV_info["s.sip2_type"]              = record[12]
    COV_info["s.sampletype"]             = record[13]
    COV_info["s.index_sample"]           = record[14]
    COV_info["s.transfer_date"]          = record[15]
    COV_info["s.comments"]               = record[16]
    COV_info["s.storing_place"]          = record[17]
    COV_info["s.seq_place"]              = record[18]
    COV_info["s.sequencing_project"]     = record[19]
    COV_info["s.shipment_shipping_code"] = record[20]
    COV_info["s.shipping_template"]      = record[21]
    COV_info["s.documentation"]          = record[22]
    COV_info["s.seq_tech"]               = record[23]
    COV_info["s.seq_library"]            = record[24]
    COV_info["s.pcr_o_c"]                = record[25]
    COV_info["s.pcr_e_c"]                = record[26]
    COV_info["s.pcr_n_c"]                = record[27]
    COV_info["s.pcr_r_c"]                = record[28]
    COV_info["s.run_code"]               = record[29]
    COV_info["s.gisaid_virusname"]       = record[30]
    COV_info["s.gisaid_epi"]             = record[31]
    COV_info["s.gisaid_authors"]         = record[32]
    COV_info["s.pcr_concentration"]      = record[33]
    COV_info["s.coverage"]               = record[34]
    COV_info["s.median_depth"]           = record[35]
    COV_info["s.lineage"]                = record[36]
    COV_info["s.clades"]                 = record[37]
    COV_info["s.mutations"]              = record[38]
    COV_info["s.branch"]                 = cutils.getTreeBranch(COV_info["s.lineage"])
    COV_info["s.birth_country"]          = record[39]
    COV_info["s.ena_accession"]          = record[40]
    COV_info["s.genbank_accession"]      = record[41]
    COV_info["s.gender"]                 = cutils.getSafeGender(record[42])
    COV_info["s.age"]                    = cutils.getSafeAge(record[43])
    COV_info["s.symptoms_onset"]         = record[44]
    COV_info["s.diagnosis"]              = record[45]
    COV_info["s.residence_city"]         = record[46]
    COV_info["s.residence_province"]     = record[47]
    COV_info["s.residence_country"]      = record[48]    
    COV_info["s.ccaa_name"]              = sp_map_utils.get_CCAA(COV_info["s.residence_province"] )
    COV_info["s.ccaa_code"]              = sp_map_utils.get_CCAA_ISO3166(COV_info["s.residence_province"])
    COV_info["s.sample_latitude"]        = record[49]
    COV_info["s.sample_longitude"]       = record[50]
    COV_info["s.hospitalization"]        = record[51]
    COV_info["s.icu_entry"]              = record[52]
    COV_info["s.percent_sarscov2"]       = record[53]
    COV_info["s.percent_homosapiens"]    = record[54]
    COV_info["h.hosp_code"]              = record[55]
    COV_info["h.name"]                   = record[56]
    COV_info["h.address"]                = record[57]
    COV_info["h.latitude"]               = record[58]
    COV_info["h.longitude"]              = record[59]
    

    # Detailed Sample date values
    dt, yy, mm, dd, epiw = cutils.getDetailedDate(COV_info["s.hospital_date"])    
    COV_info["s.hosp_date"]          = dt
    COV_info["s.hosp_date_year"]     = yy
    COV_info["s.hosp_date_month"]    = mm
    COV_info["s.hosp_date_day"]      = dd    
    COV_info["s.hosp_date_epi_week"] = epiw  # Get the EPI Week

    # Files placement
    COV_info["s.analysis_folder"]        = cutils.getAnalysisFolder(      COV_info["s.seq_place"], COV_info["s.run_code"])
    COV_info["s.sequencing_data_folder"] = cutils.getSequencingDataFolder(COV_info["s.seq_place"], COV_info["s.run_code"])

    # Local variable
    cov = COV_info["s.cov_id"]

    # Get the sample origin Province & City from the Hospital Address
    hprov, hcity, hccaa_name, hccaa_code = cutils.getInfoFromHospitalAddress(COV_info["h.address"], cov)    
    COV_info["h.ori_PROV"]  = hprov
    COV_info["h.ori_CITY"]  = hcity
    COV_info["h.ccaa_name"] = hccaa_name
    COV_info["h.ccaa_code"] = hccaa_code
    
    # If Residence City or Province are unknown, set the hospital ones plus a '_h' suffix
    COV_info["s.residence_city"]     = cutils.getSafeRCity(    COV_info["s.residence_city"],     hcity, cov, silent)
    COV_info["s.residence_province"] = cutils.getSafeRProvince(COV_info["s.residence_province"], hprov, cov, silent)
    
    # If Sample Coordinates are unknown, set the hospital ones
    COV_info["s.sample_latitude"],  COV_info["s.sample_longitude"] =       \
        cutils.getSafeSampleCoords(                                        \
            COV_info["s.sample_latitude"], COV_info["s.sample_longitude"], \
            COV_info["h.latitude"],        COV_info["h.longitude"],        \
            cov, silent)

    # All went ok
    COV_info["RESULT"] = "OK"
    return COV_info



def SQL_query(args):
    ''' 
    Connect to the DB, retrieve info for a give COV
    '''
    
    # Get only the cov code
    cov = cutils.cov2number(args.cov)
    ##cov = args.cov

    # Check cov formatting error
    if cov == -1:
        return parse_SQL_info(None, args.silent)

    # Get the SQL sentence
    sql = "SELECT "                    + \
          "s.id, "                     + \
          "s.gId, "                    + \
          "s.original_seq_id, "        + \
          "s.reception_date, "         + \
          "s.hospital_date, "          + \
          "s.status, "                 + \
          "s.project_id, "             + \
          "s.hospital_sample_number, " + \
          "s.tube_id, "                + \
          "s.sip, "                    + \
          "s.sip_type, "               + \
          "s.sip2, "                   + \
          "s.sip2_type, "              + \
          "s.sampletype, "             + \
          "s.index_sample, "           + \
          "s.transfer_date, "          + \
          "s.comments, "               + \
          "s.storing_place, "          + \
          "s.seq_place, "              + \
          "s.sequencing_project, "     + \
          "s.shipment_shipping_code, " + \
          "s.shipping_template, "      + \
          "s.documentation, "          + \
          "s.seq_tech, "               + \
          "s.seq_library, "            + \
          "s.pcr_o_c, "                + \
          "s.pcr_e_c, "                + \
          "s.pcr_n_c, "                + \
          "s.pcr_r_c, "                + \
          "s.run_code, "               + \
          "s.gisaid, "                 + \
          "s.gisaid_epi, "             + \
          "s.gisaid_authors, "         + \
          "s.pcr_concentration, "      + \
          "s.coverage, "               + \
          "s.median_depth, "           + \
          "s.lineage, "                + \
          "s.clades, "                 + \
          "s.mutations, "              + \
          "s.birth_country, "          + \
          "s.ena_accession, "          + \
          "s.genbank_accession, "      + \
          "s.gender, "                 + \
          "s.age, "                    + \
          "s.symptoms_onset, "         + \
          "s.diagnosis, "              + \
          "s.residence_city, "         + \
          "s.residence_province, "     + \
          "s.residence_country, "      + \
          "s.sample_latitude, "        + \
          "s.sample_longitude, "       + \
          "s.hospitalization, "        + \
          "s.icu_entry, "              + \
          "s.percent_sarscov2, "       + \
          "s.percent_homosapiens, "    + \
          "h.hosp_code, "              + \
          "h.name, "                   + \
          "h.address, "                + \
          "h.latitude, "               + \
          "h.longitude "               + \
          "FROM "                      + \
          " sample s INNER JOIN "      + \
          " hospital h "               + \
          "ON "                        + \
          " s.hospital_id = h.id "     + \
          "WHERE "                     + \
          " s.gId = " + str(cov) + " ;"
        # " s.hospital_sample_number LIKE \"" + str(cov) + "\" ;"
                     

    # Get all the SQL info
    SQL_info = dbqueries.Execute_SELECT(sql)

    # Pass SQL_info to a dictionary
    COV_info = parse_SQL_info(SQL_info, args.silent)

    # Return the dictionary
    return COV_info



def main():

    args     = parse_args()
    COV_info = SQL_query(args)
    cutils.print_cov_record_info(COV_info)

    return 0



if __name__ == '__main__':
    main()
