#!/usr/bin/python

'''
Generate QC stats for a sample analyzed through IBV-COVID19-iVAR pipeline. 
Takes wdepth and consensus as input to calculate depth stats and
number of 'N' in consensus
'''
#---------------------------------------------------------------
__author__      = "Galo A. Goig, Santiago Jiménez-Serrano"
__credits__     = ["Galo A. Goig", "Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import sys
import argparse
import numpy as np
import re

SCov2_annotation = {
# "SCov2_genome"    : [1, 29903],
"five_prime_UTR"  : [    1,   265],
"ORF1ab"          : [  266, 21555],
"gene_S"          : [21563, 25384],
"ORF3a"           : [25393, 26220],
"gene_E"          : [26245, 26472],
"gene_M"          : [26523, 27191],
"ORF6"            : [27202, 27387],
"ORF7a"           : [27394, 27759],
"ORF8"            : [27894, 28259],
"gene_N"          : [28274, 29533],
"ORF10"           : [29558, 29674],
"three_prime_UTR" : [29675, 29903]
}

# For the sake of speed I build a "huge" dictionary. It's possible because small genome
coord2gene = { i : "genomic" for i in range(1, 29903 + 1) } # Init to genomic
for gene in SCov2_annotation:
    start, end = SCov2_annotation[gene]
    for i in range(start, end + 1):
        coord2gene[i] = gene

def parse_args():
    '''
    Parse arguments given to script
    '''    

    parser = argparse.ArgumentParser(
        description="Generate QC stats for a sample"
        " analyzed through IBV-COVID19-iVAR pipeline. Takes wdepth and consensus as"
        " input to calculate depth stats and number of 'N' in consensus")
    parser.add_argument("-d", "--depth_file", 
                        dest="depth_file",
                        metavar=".wdepth file", 
                        help=(".wdepth file that is output of COVID19-window-coverage.py"),
                        required=True)
    parser.add_argument("-c", "--consensus",    dest="consensus",    required=True, metavar="consensu.fa")
    parser.add_argument("-H", "--print-header", dest="print_header", action="store_true")
    parser.add_argument("-s", "--sample_name",  dest="sname",        required=True)
    parser.add_argument("--min-depth",          dest="min_depth",    default=400, type=int)
    parser.add_argument("-r",                   dest="runid",        required=True)
    parser.add_argument("-v",                   dest="tsv",          required=True, metavar="variants.tsv")
    parser.add_argument("-f",                   dest="consfreq",     default=0.7, type=float, help="min freq for consensus")
    parser.add_argument("-o", "--outfile",      dest="outfile",      required=True)

    args = parser.parse_args()

    return args

def parse_wdepth(args):    

    windows_below_mindepth = {}
    windows_below_halfdepth = {}
    with open(args.depth_file) as infile:
        line = infile.readline()
        if line.startswith("Empty"):
            return (windows_below_mindepth, windows_below_halfdepth, 0, 0, 0, 0)
        if not line.startswith("Sample"):
            infile.seek(0) # If not header recover first line
        for line in infile:
            line = line.rstrip().split()
            sample, midp, window, type, anno, mean, median, o_mean, std,  o_median, IQR = line
            if float(mean) < args.min_depth:
                windows_below_mindepth[window] = (anno, type, mean, median)
            if float(mean) < float(o_mean) / 2:
                windows_below_halfdepth[window] = (anno, type, mean, median)

    return (windows_below_mindepth, windows_below_halfdepth, o_mean, std, o_median, IQR)


def parse_consensus(args):
    
    np.seterr(divide='ignore', invalid='ignore')    

    with open(args.consensus) as infile:
        infile.readline()
        # Parse genome
        genome = infile.readline().rstrip()
        nmatch = re.finditer('N+', genome)
        #nchunks = get_N_regions(genome)
        nchunks = {}
        chunk_length = []
        i = 1
        for m in nmatch:
            start = m.start()
            end = m.end()
            nlen = end - start
            chunk_length.append(nlen)
            nchunks[i] = (start + 1, end, nlen)
            i += 1

        mean = np.around(np.mean(chunk_length))
        std = np.around(np.std(chunk_length))

        ncount = genome.count("N")
        if len(genome) == 0:
            return (0, 0, 0, {}, 0, 0)

        # Count N and Ambiguous nucleotides
        ncount = 0
        ambcount = 0
        for nt in genome:
            if nt == "N":
                ncount += 1
            elif nt not in [ "A", "T", "G", "C"]:
                ambcount += 1

        else:
            perc_N = ncount / float(len(genome))
            coverage = 1 - perc_N
            # coverage = np.around(coverage)
            return (ncount, ambcount, coverage, nchunks, mean, std)

def parse_tsv(tsv_file):
    '''
    Parse ivar tsv and store in dict
    '''
    tsv = {}
    with open(tsv_file) as infile:
        HEADER = infile.readline()
        for line in infile:
            line = line.rstrip().split()
            pos = line[1]
            ALT = line[3]
            # I must calculate freqs myself as ivar way of reporting freqs is
            # very confusing, at least for the reference allele
            REF_DEPTH = int(line[4])
            ALT_DEPTH = int(line[7])
            TOTAL_DEPTH = float(line[11])
            PASS = line[13]
            REF_FREQ = REF_DEPTH / TOTAL_DEPTH
            ALT_FREQ = ALT_DEPTH / TOTAL_DEPTH
            if PASS == "TRUE":
                if pos not in tsv:
                    tsv[pos] = [ (REF_FREQ, ALT_FREQ, ALT) ]
                else:
                    tsv[pos].append( (REF_FREQ, ALT_FREQ, ALT) )

    return tsv

def count_variants(args):
    '''
    from tsv dict count snps, insertions and deletions in consensus
    '''
    tsv = parse_tsv(args.tsv)
    inscount = 0
    delcount = 0
    snpcount = 0
    for pos in tsv:
        variants = tsv[pos] # variants --> [ (REF_FREQ, ALT_FREQ, ALT), ]
        REF_FREQ  = variants[0][0] # Get REF FREQ from one of the variants
        if REF_FREQ < args.consfreq:
            # Check that REF_FREQ + all ALT_FREQS reach min consensus frequency
            # otherwise no ALT is called
            TOTAL_ALT_FREQ = sum(var[1] for var in variants)
            if REF_FREQ + TOTAL_ALT_FREQ >= args.consfreq:
                # sort all freqs at that position by ascending value
                variants = sorted(variants, key=lambda x: x[1])
                cumfreq = REF_FREQ
                while cumfreq < args.consfreq:
                    var = variants.pop()
                    ALT_FREQ = var[1]
                    ALT = var[2]
                    if "+" in ALT:
                        inscount += 1
                    elif "-" in ALT:
                        delcount += 1
                    else:
                        snpcount += 1
                    cumfreq += ALT_FREQ

    return (inscount, delcount, snpcount)

def write_depth_stats(args):

    windows_below_mindepth, windows_below_halfdepth, o_mean, o_std, o_median, o_IQR = parse_wdepth(args)
    o_mean = np.around(float(o_mean))
    o_median = np.around(float(o_median))
    o_std = np.around(float(o_std))
    o_IQR = np.around(float(o_IQR))
    ncount, ambcount, coverage, nchunks, nmean, nstd = parse_consensus(args)
    inscount, delcount, snpcount = count_variants(args)
    number_nchunks = len(nchunks)
    n_belowdepth = len(windows_below_mindepth)
    n_belowhalf  = len(windows_below_halfdepth)

    report_file = open(args.outfile, "w")

    line =  "# The consensus genome of sample {}".format(args.sname)
    line += " had {} N positions  ({})\n#".format(ncount, 1 - coverage)
    line += " in a total of {} regions".format(number_nchunks)
    line += " of an average length of {} bases (std={})\n#".format(nmean, nstd)
    line += " The genomic coverage was {}\n#".format(coverage)
    line += " The overall mean depth of the sample was {} (std={})\n#".format(o_mean, o_std)
    line += " The overall median depth of the sample was {} (IQR={})\n#".format(o_median, o_IQR)
    line += " A total of {} ambiguous nucleotides were called in consensus\n#".format(ambcount)
    line += " {} SNPs, {} insertions and {} deletions".format(snpcount, inscount, delcount)
    line += " were called in consensus\n#"
    line += " {} windows had a mean depth below".format(n_belowdepth)
    line += " the minimum cutoff established of {}\n#".format(args.min_depth)
    line += " {} windows had a mean depth below".format(n_belowhalf)
    line += " half of the mean depth of the sample ({})\n".format(o_mean)
    # line += "# NPOS={}\n".format(ncount)
    # line += "# NREGIONS={}\n".format(number_nchunks)
    # line += "# NREGION_MEAN={}\n".format(nmean)
    # line += "# MEAN={}\n".format(o_mean)
    # line += "# STD={}\n".format(o_std)
    # line += "# MEDIAN={}\n".format(o_median)
    # line += "# IQR={}\n".format(o_IQR)
    # line += "# WINDOWS_BELOW_MINDEPTH={}\n".format(n_belowdepth)
    # line += "# WINDOWS_BELOW_HALFDEPTH={}\n".format(n_belowhalf)
    report_file.write(line)

    # OUTPUT N CHUNK COORDINATES PRECEDED BY !
    line = "! REGIONS WITH UNSUFFICIENT COVERAGE (Ns) in consensus\n"
    line += "! start_coordinate:end_coordinate:length:genes_affected\n"
    for nchunk in nchunks:
        genes = set() # to know in which genes there is a lack of coverage
        start, end, nlen = nchunks[nchunk]
        for i in range(start, end+1):
            # TRY is to catch out of index problems when genomes have insertions
            try:
                gene = coord2gene[i]
                genes.add(gene)
            except KeyError:
                gene = "three_prime_UTR"
                genes.add(gene)
                warning =  "Warning: an exception has been catched "
                warning += " in sample {} with a genome ".format(args.sname)
                warning += "longer than expected. This genome has {} insertions\n".format(inscount)
                sys.stderr.write(warning)
        line += "! {}:{}:{}".format(start, end, nlen)
        for gene in genes:
            line += ":{}".format(gene)
        line += "\n"
    report_file.write(line)

    summary = "{};{};{};{};{};{};{};{};{};{};{};{}\n".format(
    args.sname,
    args.runid,
    o_mean,
    o_median,
    coverage,
    number_nchunks,
    nmean,
    ncount,
    ambcount,
    snpcount,
    inscount,
    delcount
    )
    # sys.stdout.write(HEADER)
    sys.stdout.write(summary)

    if args.print_header:
        report_file.write("sample\twindow\ttype\tannotation\tmean\tmedian\tfilter\n")

    for window in windows_below_mindepth:
        anno, type, mean, median = windows_below_mindepth[window]
        line =  "{}\t{}\t{}\t{}\t{}\t{}\tbelow_min_depth\n".format(
        args.sname,
        window,
        type,
        anno,
        mean,
        median
        )
        report_file.write(line)

    for window in windows_below_halfdepth:
        anno, type, mean, median = windows_below_halfdepth[window]
        line =  "{}\t{}\t{}\t{}\t{}\t{}\tbelow_half_depth\n".format(
        args.sname,
        window,
        type,
        anno,
        mean,
        median
        )
        report_file.write(line)

    report_file.close()

    return 0

def main():

    args = parse_args()
    write_depth_stats(args)

    return 0

if __name__ == '__main__':
    main()
