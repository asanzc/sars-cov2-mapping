#!/bin/bash
###############################################################################
## Usage (1): COVID-update-pangolin.sh <SEQPLACE> <RUNID>
## Usage (2): COVID-update-pangolin.sh -all
##
## Arguments:
##   <SEQPLACE> Sequencing place for the RUNID. E.g.: FISABIO, IBV, MAD_GM, ...
##   <RUNID>    Run identifier. E.g: MAD_GM_E0105, 200908_M02918, ...
## 
## Description:
##   (1). Updates the Pangolin lineages for the given SEQPLACE and RUNID
##   (2). Updates the Pangolin lineages for all the SEQPLACEs and RUNIDs
##
##   As results, generates the next files:
##     [...]/analysis/[...]/pangolin/Pangolin_Lineages.${RUNID}.txt
##     [...]/analysis/[...]/pangolin/lineages.${RUNID}.csv
##   
##   If -all flag is enabled, it also generates the next file:
##     ALL_Pangolin_Report.txt => Contains all the results for all the runs
##
##   To check the last lineage vesion, visit
##       https://github.com/cov-lineages/lineages (Last one 2020-05-19)
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 10/11/2020
## __version__   = 1.0.0
##
###############################################################################



function showUsage()
{
    echo -e 'Usage (1): COVID-update-pangolin.sh <SEQPLACE> <RUNID>'
    echo -e 'Description: Updates the Pangolin lineages for the given SEQPLACE and RUNID'
    echo -e 'Sample: COVID-update-pangolin.sh FISABIO 200824_M02956'
    echo -e ''
    echo -e 'Usage (2): COVID-update-pangolin.sh -all'
    echo -e 'Description: Updates the Pangolin lineages for all the SEQPLACEs and RUNIDs'
    echo -e ''	
    exit -1
}

function write_header()
{
    echo -e -n 'ID\t'           >  $1
    echo -e -n 'SEQPLACE\t'     >> $1
    echo -e -n 'RUNID\t'        >> $1
    echo -e -n 'DB_RUNID\t'     >> $1
    echo -e -n 'Lineage\t'      >> $1
    echo -e -n 'HospitalDate\t' >> $1
    echo -e -n 'HospitalCode\t' >> $1
    echo -e -n 'Province\t'     >> $1
    echo -e -n 'CCAA_code\t'    >> $1
    echo -e -n 'CCAA_name\n'    >> $1
}

function getCOVInfoField()
{
    echo $(cat $1 | grep -F $2 | cut -f2 -d':')
}



function doAnalysis()
{
    # Debug
    echo -e '\t Pangolin lineages assignment => \t SeqPlace: '$SEQPLACE'\t RunId: '$RUNID

    # Check for FISABIO seq place
    if [ "${SEQPLACE}" = "FISABIO" ]; then
    	SEQPLACE=seqfisabio
    fi

    # Set the base seqplace/runid folder
    workfolder=${BASEPATH}/${SEQPLACE}/illumina/${RUNID}

    # Check that the working folder exist
    if [ ! -d ${workfolder} ]; then
        echo -e '[ERROR] Analysis Folder does not exist -> ' ${workfolder}
        return
    fi

    # Get the real working folders
    pangolinfolder=${workfolder}'/pangolin'
    consensusfolder=${workfolder}'/consensus'

    # Create the output file paths
    FOUT01=${pangolinfolder}'/Pangolin_Lineages.'${RUNID}'.txt'
    FOUT02=${pangolinfolder}'/lineages.'${RUNID}'.csv'
    THREADS=28

    # Backup the csv file
    if [ -f ${FOUT02} ]; then
        dt=$(date +'%Y%m%d')
        faux=${FOUT02}'.'$dt'.backup'
        echo 'Backing up csv file: ' ${faux}
        mv ${FOUT02} ${faux}
    fi

    # Write the file headers
    write_header $FOUT01

    # Auxiliar fasta to be create and removed
    fna=${consensusfolder}'/consensus.'${RUNID}'.fna'

    # files filter (all samples, including no COVS)
    lsfilter=${consensusfolder}'/*.fa'

    # Create the fasta
    cat ${lsfilter} > ${fna}

    # Call pangolin
    pangolin ${fna}             \
        -o ${pangolinfolder}'/' \
        --outfile ${FOUT02}     \
        -t ${THREADS}           \
        --max-ambig 0.7

    # Remove the fasta file
    rm ${fna}

    # cov files filter (only COV values)
    lsfilter=${consensusfolder}'/COV*.fa'

    # For each COVxxxxxx fa file
    for i in $(ls ${lsfilter})
    do

        # Get the cov
        cov=$(basename $i | cut -f1 -d'.')

        # Get the lineage
        lineage=$(cat ${FOUT02} | grep ${cov} | cut -f2 -d',' )

        # Get the cov info from the DB
        ${GETCOVINFO_SCRIPT} -silent 1 -cov ${cov} > ${COVINFO}

        # Get the cov DB info related
        f1=$(getCOVInfoField ${COVINFO} s.hospital_date)
        f2=$(getCOVInfoField ${COVINFO} h.hosp_code)
        f3=$(getCOVInfoField ${COVINFO} s.residence_province)        
        f4=$(getCOVInfoField ${COVINFO} s.ccaa_code)
        f5=$(getCOVInfoField ${COVINFO} s.ccaa_name)
        f6=$(getCOVInfoField ${COVINFO} s.run_code)

        # If the ccaa is empty, set the hospital one
        if [ -z $f4 ]; then
            f3=$(getCOVInfoField ${COVINFO} h.ori_PROV)        
            f4=$(getCOVInfoField ${COVINFO} h.ccaa_code)
            f5=$(getCOVInfoField ${COVINFO} h.ccaa_name)
        fi

        # Remove the cov info auxiliar file
        rm ${COVINFO}

        # Debug?
        if [ $VERBOSE -eq 1 ]; then
            echo -e '\t\t COV:' $cov '\tLineage =' $lineage '\tCCAA =' $f4
        fi

        # Ensure valid seqplace
        splace=$SEQPLACE
        if [ "$splace" = "seqfisabio" ]; then
            splace='FISABIO'
        fi
        
        # 1 - Output the results to the output File
        echo -e $cov'\t'$splace'\t'$RUNID'\t'$f6'\t'$lineage'\t'$f1'\t'$f2'\t'$f3'\t'$f4'\t'$f5 >> $FOUT01

    done

    # Debug
    
    if [ $FLAG_ALL -eq 0 ]; then
        echo -e '\t[DONE] Results saved to -> '
        echo -e '\t ' ${FOUT01}
        echo -e '\t ' ${FOUT02}
        echo -e ''
    fi
}



# Global variables
SEQPLACE=null  # Sequencing place
RUNID=null     # Run Id
FLAG_ALL=1     # 1=YES, 0=NO
VERBOSE=1      # 1=YES, 0=NO
BASEPATH='/data2/COVID19/analysis'
GETCOVINFO_SCRIPT='/data2/COVID19/software/sars-cov2-mapping/utils/COVID_getCOVSampleInfo.py'
COVINFO='./cov.info'
OUTFILE01='./ALL_Pangolin_Report.txt'

# Pangoling env
PANG_ENV='/data2/COVID19/software/miniconda3/envs/pangolin'

if [ $# -eq 2 ];then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    FLAG_ALL=0   # Only the specified seq place and runID
    VERVOSE=1    # In this mode, the verbose mode is on
elif [ "$1" = "-all" ]; then
    FLAG_ALL=1
    VERBOSE=0    # In this mode the verbose mode is off
else
	echo -e '\n[ERROR] Wrong number of arguments or incorrect use of them \n'
	showUsage
fi


# Perform only one reanalysis for the given seqplace & run id
if [ $FLAG_ALL -eq 0 ]; then
    source activate ${PANG_ENV}
    doAnalysis
    conda deactivate
    exit 0
fi

# Check for errors
if [ $FLAG_ALL -ne 1 ]; then
    echo '[ERROR]: Unknown command options'
    showUsage
fi

#############################################################
# From here, it is assumed the -all flag ####################
#############################################################

# Reset the out file
write_header $OUTFILE01

# Activate conda environment
source activate ${PANG_ENV}

# For each sequencing place folder
for seq_place in $(ls $BASEPATH)
do

    # Get the full path to the Sequencing Place
    path01=${BASEPATH}/$seq_place

    # Ensure directory
    if [ ! -d $path01 ]; then
        continue
    fi

    # Jump general_results
    if [ "$seq_place" = "general_results" ]; then
        continue
    fi

    # Get a new variable with the Real Sequencing Place
    real_seq_place=$seq_place

    # FISABIO seq place exception
    if [ "$seq_place" = "seqfisabio" ]; then
        real_seq_place="FISABIO"
    fi    

    # For each run folder
    for runid in $(ls $path01/illumina)
    do
        path02=$path01/illumina/$runid

        # Ensure directory
        if [ ! -d $path02 ]; then
            continue
        fi

        # Set the global variables and perform the analysis
        SEQPLACE=$real_seq_place  # Sequencing place
        RUNID=$runid
        doAnalysis

        # Write to the total output files jumping the header        
        tail -n +2 ${FOUT01} >> ${OUTFILE01}
        
    done # end runid

done # end seqplace

# Deactivate conda environment
conda deactivate


# Debug
echo -e '[FINAL DONE] Total results saved to -> '
echo -e '\t ' ${OUTFILE01}
echo -e ''
