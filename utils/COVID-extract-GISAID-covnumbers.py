#! /usr/bin/python
# -*- coding: utf-8 -*-
'''
Parse GISAID virus names from GISAID metadata as given by manual 
curators and extract our COV ID number plus GISAID accession
'''
#---------------------------------------------------------------
__author__      = "Galo A. Goig, Santiago Jiménez-Serrano"
__credits__     = ["Galo A. Goig", "Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import argparse
import sys



consortium_submitters = [
"SeqCOVID-SPAIN consortium/IBV(CSIC)",
"Sequencing and Bioinformatics Service and Molecular Epidemiology Research Group. FISABIO-Public Health",
"Sequencing and Bioinformatics Service and Molecular Epidemiology Research Group. FISABIO-Public Health.",
"Sequencing and Bioinformatics Service and Molecular Epidemiology Research Group. FISABIO-Public Health, and SeqCOVID-Spain Consortium",
"Sequencing and Bioinformatics Service FISABIO-Public Health",
"Sequencing and Bioinformatics Service. Molecular Epidemiology Laboratory. FISABIO-Public Health"
]



def parse_args():
    '''
    Parse arguments given to script
    '''

    parser = argparse.ArgumentParser(description="Extract COV ID from GISAID virus name")
    parser.add_argument("-m", "--metadata", dest="mfile", required=True)
    args = parser.parse_args()
    return args



def parse_virus_name(virus_name):
    # virus name = Spain/Albacete-COV003767/2020
    virus_name = virus_name.split("/")
    # virus name = [Spain, Albacete-COV003767, 2020]
    covnumber = virus_name[1]
    while not covnumber.isdigit():
        covnumber = covnumber[1:] # remove first char from string until only number
    COV = "COV{:0>6}".format(covnumber) # 234 to COV000234

    return COV



def parse_metadata(args):
    sys.stdout.write("ID,GISAID ID\n")
    with open(args.mfile) as infile:
        for line in infile:
            line = line.rstrip().split("\t")
            virus_name = line[0]
            accession = line[2]
            submitting_lab = line[20]
            if accession != "EPI_ISL_429256": # This accession is from FISABIO but not from the consortium
                if submitting_lab in consortium_submitters:
                    COV = parse_virus_name(virus_name)
                    # Las muestras subidas a GISAID empiezan a tener un identificador
                    # COV a partir del COV000470. Por lo tanto el accession anterior
                    # de esas muestras que son de Valencia tendrá que recuperarse de
                    # otra manera
                    if int(COV.lstrip("COV")) >= 470:
                        sys.stdout.write("{},{}\n".format(COV, accession))

    return 0



def main():
    args = parse_args()
    parse_metadata(args)
    return 0



if __name__ == '__main__':
    main()
