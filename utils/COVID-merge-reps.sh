#! /usr/bin/bash
ls | grep -i rep2 | 
grep -o "COV[0-9]\{6\}" |
sort -u > Resequenced_samples.txt

cat Resequenced_samples.txt | while read COV
 do 
  # Search all previously sequenced samples discarding previously
  # merged like COVXXXXXm <- Also at this stage we to not take into
  # account samples already filtered with Kraken, just for convenience
  # The merged sampled will be classified as a whole again and filtered
  # accordingly
  find /data2/COVID19/sequencing_data/ -name "${COV}*.fastq.gz" | 
  grep -v "COV[0-9]\{6\}m" | grep -v "humanfilt" | grep R1 |
  while read FFQ
    do
      zcat ${FFQ}
    done | gzip > ${COV}m.R1.fastq.gz
  # Now the same for reverse reads files 
  find /data2/COVID19/sequencing_data/ -name "${COV}*.fastq.gz" | 
  grep -v "COV[0-9]\{6\}m" | grep -v "humanfilt" | grep R2 |
  while read RFQ
    do
      zcat ${RFQ}
    done | gzip > ${COV}m.R2.fastq.gz
    echo "${COV};${COV}m.R1.fastq.gz;${COV}m.R2.fastq.gz"
done > master_table_merged.txt
