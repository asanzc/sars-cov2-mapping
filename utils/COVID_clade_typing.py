#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Script for typing SARS-CoV-2 genomes. 

It uses the tsv files derived from the IVAR pipeline
and a tab file with all the clade-defining mutations. 
To classify a sample as belonging to a clade, request 
at least one clade-defining mutation to be present. 
Genomic position and allelic change must match.
'''

#---------------------------------------------------------------
__author__      = "Álvaro Chiner-Oms"
__credits__     = ["Álvaro Chiner-Oms"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import argparse
import pandas
import numpy


#################################################################
# Constant definitions
#################################################################
DEFAULT_CLADES_FILE='/data2/COVID19/reference_data/mutations_for_typing/clade_mutations.csv'
DEFAULT_MUTATS_FILE='/data2/COVID19/reference_data/mutations_for_typing/mutations.csv'
#################################################################


#################################################################
# Arguments parsing
#################################################################

def parse_args():
    '''
    Parse arguments given to script
    '''
        
    parser = argparse.ArgumentParser(
        description='Typing SARS-CoV-2 samples from a VCF file')

    parser.add_argument('-t', '--typ-table', 
        dest     = 'typ_table',
        required = False,
        default  = DEFAULT_CLADES_FILE,
        help     = 'Tab separated table with the mutations that define the clades of interest')

    parser.add_argument('-v', '--vcf_file', 
        dest='vcf_file',
        required=True,
        help='VCF file containing the variants called after running the pipeline')

    parser.add_argument('-m', '--mut-table', 
        dest     ='mut_table', 
        required = False,
        default  = DEFAULT_MUTATS_FILE,
        help     ='Tab separated table with the mutations wanted to be surveillanced')

    args = parser.parse_args()
    
    return args


#################################################################
# Clade typing
#################################################################

def getClades_FromPositions(vcf_path, table = DEFAULT_CLADES_FILE):
    '''
    Function to extract information from the VCF file and type. 
    Only SNPs with a freq >= 80% and >= 30X are used for typing.
    A positions-allele match is required.
    '''

    # Read the VCF
    vcf_table = pandas.read_csv(vcf_path, sep='\t') 

    # Only positions having more than 30X
    depth_filter = vcf_table['TOTAL_DP'] >= 30

    # Only alleles above 80% freq
    freq_filter = vcf_table['ALT_FREQ'] >= 0.8

    # Make a copy of the rows of interest, to avoid warning
    vcf_table_filtered = pandas.DataFrame(vcf_table[freq_filter & depth_filter].copy()) 

    # Read the table
    typing_table = pandas.DataFrame(pandas.read_csv(table, sep='\t'))

    # Resulting output
    result    = ''
    aux       = ''
    nclades   = 0
    separator = '&'

    # For each row
    for row in typing_table.itertuples():

        # If any of the TSV rows agrees with a
        # row of the list in POSITION and ALLELE 
        if not vcf_table_filtered[(vcf_table_filtered['POS'] == row.POSITION) & \
           (vcf_table_filtered['ALT'] == row.ALLELE)].empty:

            # Check that it has been detected by other mutation
            # (at least two mutations for typing a clade)
            # and that the typing is not duplicated 
            if row.CLADE in aux and row.CLADE not in result: 
                result   = result + row.CLADE + separator
                nclades += 1            
            aux = aux + row.CLADE + separator

    # No clades found code
    if nclades == 0:
        return 'NA'

    # Remove last separator
    result = result [:-1]

    # Get the result
    return result



#################################################################
# Clade typing
#################################################################

def getMutations_FromPositions(vcf_path, table = DEFAULT_MUTATS_FILE):
    '''
    Function to extract information from the VCF file and look
    for mutations of interest. 
    Only SNPs with a freq >= 5% and >= 30X are used for typing.
    A positions-allele match is required.
    '''

    # Read the VCF
    vcf_table = pandas.read_csv(vcf_path, sep='\t') 

    # Only positions having more than 30X
    depth_filter = vcf_table['TOTAL_DP'] >= 30 

    # Only alleles above 80% freq
    freq_filter = vcf_table['ALT_FREQ'] >= 0.05 

    # Make a copy of the rows of interest, to avoid warning
    vcf_table_filtered = pandas.DataFrame(vcf_table[freq_filter & depth_filter].copy())

    # Read the table
    typing_table = pandas.DataFrame(pandas.read_csv(table, sep='\t')) 

    # Resulting output
    result=''
    nmuts=0
    separator = '&'

    # For each row
    for row in typing_table.itertuples(): 

        # If any of the TSV rows agrees with a row
        # of the list in POSITION and ALLELE 
        if not vcf_table_filtered[(vcf_table_filtered['POS'] == row.POSITION) & \
           (vcf_table_filtered['ALT'] == row.ALLELE)].empty: 

            # Get & round the value
            value = vcf_table_filtered[(vcf_table_filtered['POS'] == row.POSITION) & (vcf_table_filtered['ALT'] == row.ALLELE)].iloc[0,10]
            value = round(value, 2)

            # Append the result
            result = result \
                + row.MUTATION \
                + '(' \
                + str(value) \
                + ')' + separator
            nmuts += 1


    # No mutations found code
    if nmuts == 0:
        return 'NA'

    # Remove last separator
    result = result [:-1]

    # Get the result
    return result


#################################################################
# Utils
#################################################################


def getCov_FromVcfPath(vcf_path):
    '''
    Remove the path folders and extensions for the given path
    '''

    # Initially, copy the path
    cov = vcf_path

    # Remove folders from path, keeping last element
    if cov.find("/") != -1:        
        cov = cov.rsplit("/",1)[1]
    
    # Remove de '.tsv' extension.
    cov = cov.replace('.tsv', '')

    # Return the COV (or file identifier)
    return cov



#################################################################
# Main functions
#################################################################

def main():
    '''
    Main entry point
    '''
    
    # Parse args
    args = parse_args()

    # Get the clade list
    clade_list = getClades_FromPositions(args.vcf_file, args.typ_table)

    # Get the mutations list
    mutations_list = getMutations_FromPositions(args.vcf_file, args.mut_table)

    # Get the corresponing COV (of file id)
    cov = getCov_FromVcfPath(args.vcf_file)
    
    # Print results
    print(cov, '\t', clade_list, '\t', mutations_list)
    
    
if __name__ == "__main__":
    main()


