 #!/usr/bin/sh
###############################################################################
#                       ENA SUBMISSION SCRIPT
#       Reads manifest files from current folder and submit to ENA
#       Webin User and Pass are read from config file
###############################################################################
# Get absolute path of the current script
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $SCRIPT_DIR/../pipeline_parameters.config

# First we have to check whether a previous submission has been attempted
# and in that case only submit those that have not been successfully submited
if [ ! -d "./reads/" ]; then # if reads/ directory does not exist no previous attempt
  echo "reads/ directory does not exist. Starting batch submission"
  echo "ID,ENA accession" > COV_ENA.accessions
  ls *HiQ.ENA\_manifest | cut -f1 -d'.' |
    while read COV
    do
      java -jar ${WEBIN} -manifest ${COV}.HiQ.ENA\_manifest \
      -context reads -submit \
      -userName ${WUSER} -password ${WPASS}
      ACC=`grep "RUN accession" ./reads/${COV}/submit/receipt.xml | cut -f2 -d'"'`
      echo "${COV},${ACC}" >> COV_ENA.accessions
    done
else # try to submit COVS that have not been already submitted
   echo "reads/ directory exists. Resuming submission"
   find -name "*receipt.xml" |
    xargs grep "Submission has been committed" |
     grep -o "COV[0-9][0-9][0-9][0-9][0-9][0-9]" > COVS_already_submitted.txt
   find -name "*receipt.xml" |
    xargs grep "The object being added already exists" |
     grep -o "COV[0-9][0-9][0-9][0-9][0-9][0-9]" >> COVS_already_submitted.txt
   sort -u COVS_already_submitted.txt > tmp && mv tmp COVS_already_submitted.txt
   ls *HiQ.ENA\_manifest | cut -f1 -d'.' |
     while read COV
     do
       grep ${COV} COVS_already_submitted.txt > /dev/null
       if [ ${?} != "0" ]; then # if COV not found in already submitted
         java -jar ${WEBIN} -manifest ${COV}.HiQ.ENA\_manifest \
         -context reads -submit \
         -userName ${WUSER} -password ${WPASS}
         if [ ${?} == "0" ]; then # If submission completes
            ACC=`grep "RUN accession" ./reads/${COV}/submit/receipt.xml | cut -f2 -d'"'`
            echo "${COV},${ACC}" >> COV_ENA.accessions
         fi
         else echo "${COV} has been already submitted and will be ignored"
       fi
    done
fi
