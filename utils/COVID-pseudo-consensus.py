#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
Script for constructing a pseudo-consensus from a reference genome and the VCF file
'''
#---------------------------------------------------------------
__author__      = "Galo A. Goig, Santiago Jiménez-Serrano"
__credits__     = ["Galo A. Goig", "Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------

import argparse

def parse_args():
    '''
    Parse arguments given to script
    '''
    

    parser = argparse.ArgumentParser(description="Construct a pseudoconsensus from a"
    "reference genome and a VCF file")
    parser.add_argument("-r", "--reference", dest="reference_genome", required=True)
    parser.add_argument("-v", "--vcf_file", dest="vcf_file", required=True,
        help=("VCF file containing the variants called after running the pipeline"))
    parser.add_argument("-d", "--depth_file", dest="depth_file", required=True,
        help=("Tabular file containing information about the depth of coverage per position"))
    parser.add_argument("-o", "--output_filename", dest="outfile",
                        required=True)
    args = parser.parse_args()

    return args


def extract_data_VCF(vcf):
    '''
    Extract information from the VCF file. 
    Those SNPs with a freq [5%-80%] are transformed into ambiguous bases
    '''

    import pandas

    dict_hetero = {
        "GA":"R", "AG":"R", "TC":"Y", "CT":"Y", "GT":"K", "TG":"K",
        "AC":"M", "CA":"M", "GC":"S", "CG":"S", "AT":"W", "TA":"W"
    }

    vcf_table = pandas.read_csv(vcf, sep="\t") #read the VCF
    depth_filter = vcf_table['TOTAL_DP'] >= 30   #only positions having more than 30X
    freq_filter  = vcf_table['ALT_FREQ'] >= 0.05 #only alleles bove 80% freq
    vcf_table_filtered = vcf_table[freq_filter & depth_filter].copy() #make a copy of the rows of interest, to avoid warning

    for index,row in vcf_table_filtered.iterrows(): #for each row
        if row.ALT_FREQ < 0.8 and row.ALT[0] != '-' and row.ALT[0] != '+': #if the alt allele has a freq < 80%, and is not and insertion or a deletion
            vcf_table_filtered.at[index,'ALT'] = dict_hetero.get(row.ALT + row.REF) #the alt allele becames an ambiguous base

    vcf_table_filtered = vcf_table_filtered.sort_values(by=['ALT_DP'], ascending = False) #sort data by ALT_DP

    return vcf_table_filtered.POS.tolist(), vcf_table_filtered.REF.tolist(), vcf_table_filtered.ALT.tolist()


def extract_data_DEPTH(vcf):
    """Function to extract information from the DEPTH file"""

    import pandas

    vcf_table = pandas.read_csv(vcf, sep="\t", names=['Id','position','depth']) #read the VCF
    depth_filter = vcf_table['depth'] < 30 #only positions having less than 30X

    return vcf_table.position[depth_filter].tolist()


def replacer_SNPs(s, allele, pos, reference):
    """Function to replace characters in a specific string position"""

    dict_triallelic = {
        "YG":"B", "GY":"B", "KC":"B", "CK":"B", "ST":"B", "TS":"B",
        "WG":"D", "GW":"D", "KA":"D", "AK":"D", "RT":"D", "TR":"D",
        "YA":"H", "AY":"H", "WC":"H", "CW":"H", "MT":"H", "TM":"H",
        "GM":"V", "MG":"V", "RC":"V", "CR":"V", "SA":"V", "AS":"V",
        "RY":"N", "YR":"N", "RK":"D", "KR":"D", "RM":"V", "MR":"V",
        "RS":"V", "SR":"V", "RW":"D", "WR":"D", "YK":"B", "KY":"B",
        "YM":"H", "MY":"H", "YS":"B", "SY":"B", "YW":"H", "WY":"H",
        "KM":"N", "MK":"N", "KS":"B", "SK":"B", "KW":"D", "WK":"D",
        "MS":"V", "SM":"V", "MW":"H", "WM":"H", "SW":"N", "WS":"N"
    }

    # raise an error if index is outside of the string
    if pos not in range(len(s)):
        raise ValueError("Position " + str(pos) + " outside of the SARS-CoV-2 genomic range.")

    if s[pos-1] != reference: #happens when a substitution have been already carried out

        if s[pos-1] in ["-","N"]: #over a deletion
            if allele[0] == '-': #other deletion
                return s[:pos] + 'N'*(len(allele)-1) + s[pos+(len(allele)-1):] # put N
            elif allele[0] == '+': #an insertion, skip
                return s
            else: #a SNP
                return s[:pos-1] + 'N' + s[pos:] # put N

        elif allele[0] == '-': #over a substitution, a deletion
            return s[:pos] + 'N'*(len(allele)-1) + s[pos+(len(allele)-1):] # put N

        elif s[pos-1] in ["A","G","T","C","R","Y","K","M","S","W"] and allele[0] != '+': #over a previous substitution, not a deletion neither an insertion, probably a triallelic position
            return s[:pos-1] + dict_triallelic.get(allele + s[pos-1]) + s[pos:]

        else: #insertions, skip
            return s

    else: # if no substitution have been carried out in the selected base

        if allele[0] == '-': #deletions. In this case, position is the previous genomic position.
            return s[:pos] + '-'*(len(allele)-1) + s[pos+(len(allele)-1):]

        elif allele[0] == '+':#insertions. Do nothing
            return s

        else: #snps
            return s[:pos-1] + allele + s[pos:]


def main():
    from Bio import SeqIO
    from Bio.SeqRecord import SeqRecord
    from Bio.Seq import Seq
    from Bio.Alphabet import IUPAC

    args = parse_args()
    record = SeqIO.read(args.reference_genome, "fasta")
    sequence = record.seq #the genomic sequence of the reference

    if args.vcf_file.find("/") != -1:
        cov = args.vcf_file.rsplit("/",1)[1].replace('.tsv','') #rsplit for splitting large paths containing '/'. Keep last element. For this last element, remove de '.tsv' extension.
    else:
        cov = args.vcf_file.replace('.tsv','')

    positions_snp, ref_alleles, alt_alleles = extract_data_VCF(args.vcf_file) #modify alternative alleles
    for i in range(len(positions_snp)):
        sequence = replacer_SNPs(sequence, alt_alleles[i], positions_snp[i], ref_alleles[i])

    positions_n = extract_data_DEPTH(args.depth_file) #modify bases with low coverage
    for p in positions_n:
        sequence = sequence[:p-1] + 'N' + sequence[p:]

    new_sequence = SeqRecord(Seq(str(sequence), IUPAC.IUPACAmbiguousDNA), cov, description="")
    SeqIO.write(new_sequence, args.outfile, "fasta")


if __name__ == "__main__":
    main()
