#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Generate QC summary stats after an analysis run
'''
#---------------------------------------------------------------
__author__      = "Galo A. Goig, Santiago Jiménez-Serrano"
__credits__     = ["Galo A. Goig", "Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import os
import sys
import argparse
import numpy as np
import re
import COVID_Utils        as cutils
import COVID_SQL_Queries  as dbqueries
import COVID_clade_typing as cladet


#########################################################
# Global variables
#########################################################

SCov2_annotation = {
# "SCov2_genome"    : [1, 29903],
"five_prime_UTR"  : [    1,   265],
"ORF1ab"          : [  266, 21555],
"gene_S"          : [21563, 25384],
"ORF3a"           : [25393, 26220],
"gene_E"          : [26245, 26472],
"gene_M"          : [26523, 27191],
"ORF6"            : [27202, 27387],
"ORF7a"           : [27394, 27759],
"ORF8"            : [27894, 28259],
"gene_N"          : [28274, 29533],
"ORF10"           : [29558, 29674],
"three_prime_UTR" : [29675, 29903]
}

# For the sake of speed a "huge" dictionary is built
# It's possible because small genome
coord2gene = { i : "genomic" for i in range(1, 29903 + 1) } # Init to genomic
for gene in SCov2_annotation:
    start, end = SCov2_annotation[gene]
    for i in range(start, end + 1):
        coord2gene[i] = gene



#########################################################
# Args parsing
#########################################################

def parse_args():
    '''
    Parse the arguments given to the script
    '''    

    parser = argparse.ArgumentParser(
        description="Generate QC summary stats after an analysis run")

    # Master table
    parser.add_argument("-m", "--master_table", dest="master_table",
    metavar="MASTER TABLE", required=True)

    # Run/working folder
    parser.add_argument("--run-directory", dest="rundir",
    help=("Folder where the anaysis run has been performed. From this folder "
    "SARS-CoV-2 lineages and other data is read to generate Microreact metadata"),
    required=True)

    # Run id
    parser.add_argument("--run-id", dest="runid", required=True)

    # Out file
    parser.add_argument("-o", "--outfile", dest="outfile", required=True)

    # All samples mode (not only COVs)
    parser.add_argument('-all', dest='all', required=False, default='False',
        help='Indicates if analyze all the samples in the run, including the non COVs')

    # Gets & returns the args object
    args = parser.parse_args()
    return args


#########################################################
# Basic functions
#########################################################

def list_result_files(args):
    '''
    List the results files for this run
    '''

    # Get the full path folder
    rundir = os.path.abspath(args.rundir)

    # Check for errors
    if not os.path.exists(rundir):
        sys.exit("Error: the run directory specified does not exist\n")

    # For each of the samples, include in a 
    # dictionary their result files associated
    # (consensus, variants, wdepth, ...)
    covs = parse_master_table(args)

    # Create dictionary with COVS pointing to result files
    result_files = {
                    cov : {"consensus": "{}/consensus/{}.fa".format(rundir,cov),
                           "variants" : "{}/variants/{}.tsv".format(rundir,cov),
                           "wdepth"   : "{}/reports/{}.wdepth".format(rundir, cov),
                           "fastp"    : "{}/reports/{}_fastp.json".format(rundir, cov),
                           "adepth"   : "{}/reports/{}.adepth".format(rundir, cov),
                           "report"   : "{}/reports/{}.report".format(rundir, cov)
                           } for cov in covs }

    # Retrieve lineages from pangolin file
    pangolin_file = "{}/pangolin/lineages.{}.csv".format(rundir, args.runid)

    # Retrieve run_log_file to get consensus freq and min depth
    run_logfile = "{}/logfolder/RUN.{}.log".format(rundir, args.runid)

    # Return the tuple of results
    return (result_files, pangolin_file, run_logfile)


#########################################################
# Group COV info file parsing
#########################################################

def parse_master_table(args):
    '''
    Gets the list of COVS from the run master_table
    '''

    # Initialize the list
    covs = []

    # Set the separator char
    separator = ';'

    # Open the file
    with open(args.master_table) as infile:
        
        # For each line
        for line in infile:

            # Jump empty or invalid lines
            if separator in line:

                # m just in case is resequencing
                cov = line.split(separator)[0].rstrip("m")

                # Check the -all mode: only COVs?
                if not args.all == 'False' or cov.startswith("COV"):
                    covs.append(cov)

    return covs


def parse_lineages(pangolin_file):
    '''
    Parse the Pangolin lineages file for all the runid
    '''

    lineages = {}
    with open(pangolin_file) as infile:

        # Skip header
        infile.readline()

        # Read each line
        for line in infile:

            # Get the COV code and lineage
            COV, lineage, _ = line.split(",", 2)

            # Check for some errors in the key
            if "COV" in COV and len(COV) != 9:
                index = COV.index('COV')
                COV = COV[index: index+9]

            # Set the lineage
            lineages[COV] = lineage

    return lineages


def parse_logfile_parameters(run_logfile):
    '''
    Get basic parameters used in run analysis from logfile
    '''
    parameters = { }
    with open(run_logfile) as infile:
        for line in infile:
            line = line.strip()
            if not line.startswith("=") and line != "":
                first_token = line.split()[0]
                if "=" in first_token:
                    parameter, value = first_token.split("=")
                    parameters[parameter] =  value
    return parameters


#########################################################
# Single COV info file parsing
#########################################################

def parse_fastp(fastp_json):
    '''
    Read the number of reads from fastq files from fastp_json (VERY NAIVE)
    '''

    # Check if file exists
    if not os.path.isfile(fastp_json):
        return int(0)

    # Read fastp file
    with open(fastp_json) as infile:
        for line in infile:
            line = line.strip()
            if line.startswith('"total_reads"'):
                reads = int(line.split(":")[1].rstrip(","))
                return int(reads / 2)


def parse_wdepth(depth_file, min_depth):
    '''
    Parse wdepth file for a given sample (COV or not)
    '''

    min_depth = int(min_depth)
    windows_below_mindepth = {}
    windows_below_halfdepth = {}
    with open(depth_file) as infile:

        # Read header
        line = infile.readline()
        if line.startswith("Empty"):
            return (windows_below_mindepth, windows_below_halfdepth, 0, 0, 0, 0)
        if not line.startswith("Sample"):
            infile.seek(0) # If not header recover first line

        # Read each line
        for line in infile:
            line = line.rstrip().split()
            sample, midp, window, type, anno, mean, median, o_mean, std,  o_median, IQR = line
            o_mean = np.around(float(o_mean))
            o_median = np.around(float(o_median))
            std = np.around(float(std))
            IQR = np.around(float(IQR))
            if float(mean) < min_depth:
                windows_below_mindepth[window] = (anno, type, mean, median)
            if float(mean) < float(o_mean) / 2:
                windows_below_halfdepth[window] = (anno, type, mean, median)

    return (windows_below_mindepth, windows_below_halfdepth, o_mean, std, o_median, IQR)


def parse_adepth(adepth_file):
    '''
    Parse adepth file for a given sample (COV or not)
    '''

    # Check that file exist
    if not os.path.isfile(adepth_file):
        w = "[WARNING] adepth file does not exist: {} \n".format(adepth_file)
        sys.stderr.write(w)
        return 0.0

    # Open the adepth file
    with open(adepth_file) as infile:

        # Skip header
        header = infile.readline() 
        if not header:
            return 0

        # First line has median depth for pool 1
        pool_1 = float(infile.readline().rstrip().split(";")[-1])

        # skip next line with average depth for pool 1
        infile.readline()

        # third line has median depth for pool 2
        pool_2 = float(infile.readline().rstrip().split(";")[-1])

        # Avoid NaN in division
        if pool_2 == 0:
            pool_2 = 0.0000000001

        # Gets the ratio
        ratio = pool_1 / pool_2

        # Return the ration
        return round(ratio, 6)


def parse_consensus(consensus_file):
    '''
    Parse consensus file for a given sample (COV or not)
    '''

    # Avoid warnings
    np.seterr(divide='ignore', invalid='ignore')    

    # Read consensus file
    with open(consensus_file) as infile:

        # Skip header
        infile.readline()

        # Parse genome
        genome = infile.readline().rstrip()
        nmatch = re.finditer('N+', genome)
        #nchunks = get_N_regions(genome)
        nchunks = {}
        chunk_length = []
        i = 1
        for m in nmatch:
            start = m.start()
            end = m.end()
            nlen = end - start
            chunk_length.append(nlen)
            nchunks[i] = (start + 1, end, nlen)
            i += 1

        mean = np.around(np.mean(chunk_length))
        std = np.around(np.std(chunk_length))

        # Check for errors
        if len(genome) == 0:
            return (0, 0, 0, {}, 0, 0, 0)

        # Count N and Ambiguous nucleotides
        ncount = 0
        ambcount = 0
        for nt in genome:
            if nt == "N":
                ncount += 1
            elif nt not in [ "A", "T", "G", "C"]:
                ambcount += 1

        # Get the coverage
        perc_N = ncount / float(len(genome))
        coverage = 1 - perc_N
        coverage = round(coverage, 4)

        # Gets the number of chunks
        number_nchunks = len(nchunks)

        # Get the results tuple
        return (ncount, ambcount, coverage, nchunks, mean, std, number_nchunks)


def parse_tsv(tsv_file):
    ''' 
    Parse ivar tsv file for a given sample and store in dict
    '''
    tsv = {}
    with open(tsv_file) as infile:

        # Skip header
        _ = infile.readline()

        # Read each line
        for line in infile:
            line = line.rstrip().split()
            pos = line[1]
            ALT = line[3]
            # I must calculate freqs myself as ivar way of reporting freqs is
            # very confusing, at least for the reference allele
            REF_DEPTH = int(line[4])
            ALT_DEPTH = int(line[7])
            TOTAL_DEPTH = float(line[11])
            PASS = line[13]
            REF_FREQ = REF_DEPTH / TOTAL_DEPTH
            ALT_FREQ = ALT_DEPTH / TOTAL_DEPTH
            if PASS == "TRUE":
                if pos not in tsv:
                    tsv[pos] = [ (REF_FREQ, ALT_FREQ, ALT) ]
                else:
                    tsv[pos].append( (REF_FREQ, ALT_FREQ, ALT) )

    return tsv


def parse_variants(variants_file, consfreq):
    '''
    From tsv dict count snps, insertions and deletions in consensus
    Also, gets the clade types and mutations
    '''

    # Initialize the local variables
    consfreq = float(consfreq)
    inscount = 0
    delcount = 0
    snpcount = 0    
    clades = 'NA'
    mutations   = 'NA'

    # Check if file exists
    if not os.path.isfile(variants_file):
        w = "[WARNING] variants file does not exist: {} \n".format(variants_file)
        sys.stderr.write(w)
        return (inscount, delcount, snpcount, clades, mutations)

    # Get the Clades and mutations (using auxiliar script)
    clades    = cladet.getClades_FromPositions(variants_file)
    mutations = cladet.getMutations_FromPositions(variants_file)

    # Parse the tsv file
    tsv = parse_tsv(variants_file)

    # For each position in the tsv
    for pos in tsv:
        variants = tsv[pos] # variants --> [ (REF_FREQ, ALT_FREQ, ALT), ]
        REF_FREQ  = variants[0][0] # Get REF FREQ from one of the variants
        if REF_FREQ < consfreq:
            # Check that REF_FREQ + all ALT_FREQS reach min consensus frequency
            # otherwise no ALT is called
            TOTAL_ALT_FREQ = sum(var[1] for var in variants)
            if REF_FREQ + TOTAL_ALT_FREQ >= consfreq:
                # sort all freqs at that position by ascending value
                variants = sorted(variants, key=lambda x: x[1])
                cumfreq = REF_FREQ
                while cumfreq < consfreq:
                    var = variants.pop()
                    ALT_FREQ = var[1]
                    ALT = var[2]
                    if "+" in ALT:
                        inscount += 1
                    elif "-" in ALT:
                        delcount += 1
                    else:
                        snpcount += 1
                    cumfreq += ALT_FREQ

    return (inscount, delcount, snpcount, clades, mutations)


def parse_report(report_file):
    '''
    Parse the report file for a given sample in order 
    to get the % of sars-cov2 and HomoSapiens
    '''

    perc_sars = 'unknown'
    perc_homo = 'unknown'

    if not os.path.isfile(report_file):
        return (perc_sars, perc_homo)

    perc_sars_readed = False
    perc_homo_readed = False

    with open(report_file) as infile:
        for line in infile:
            line = line.strip()

            # Read % of sars-cov2 ? (read first entry)
            if 'Betacoronavirus' in line and not perc_sars_readed:
                perc_sars = line.split('\t')[0].strip()
                perc_sars_readed = True
            
            # Read % of Homo sapiens ? (read first entry)
            elif 'Homo sapiens' in line and not perc_homo_readed:
                perc_homo = line.split('\t')[0].strip()
                perc_homo_readed = True

            if perc_sars_readed and perc_homo_readed:
                return (perc_sars, perc_homo)

    # Not found some of the fields
    return (perc_sars, perc_homo)



#########################################################
# Database query
#########################################################

def read_DB():
    '''
    Retrieve Origin hospital and Ct values for each cov
    '''

    # Get the SQL sentence
    sql = \
        "SELECT "            + \
        " s.gId, "           + \
        " s.pcr_e_c, "       + \
        " s.pcr_r_c, "       + \
        " s.pcr_n_c, "       + \
        " s.hospital_date, " + \
        " h.name "           + \
        " FROM sample s "    + \
        " INNER JOIN hospital h " + \
        " ON s.hospital_id = h.id"

    # Run the sql query
    SQL_info = dbqueries.Execute_SELECT(sql)

    # Initialize result dictionary
    DB = {}
    
    # For each row
    for record in SQL_info:
        
        # Get fields for each column
        cov      = cutils.number2cov(record[0])
        pcr_e_c  = cutils.getSafeRoundedFloat(record[1])
        pcr_r_c  = cutils.getSafeRoundedFloat(record[2])
        pcr_n_c  = cutils.getSafeRoundedFloat(record[3])
        date     = cutils.getSafeDate(record[4])
        hospital = record[5].replace(" ", "_")

        # Check errors
        if cov in DB:
            sys.stderr.write("WTF? A duplicated COV in the DB?!?!")

        # Save record in dictionary
        DB[cov] = { 
            "pcr_e_c"  : pcr_e_c,
            "pcr_r_c"  : pcr_r_c,
            "pcr_n_c"  : pcr_n_c,            
            "date"     : date,
            "hospital" : hospital
        }

    # Return the full dictionary
    return DB


#########################################################
# QC Summary generation
#########################################################

def write_summary(args):

    # Get the sequencing place according to the run_dir
    seqplace = cutils.getSeqPlaceFromPath(args.rundir)

    # Get the path for the files to be analyzed
    result_files,  \
    pangolin_file, \
    run_logfile = list_result_files(args)

    # Read the pangolin lineages
    lineages = parse_lineages(pangolin_file)

    # Read the run parameters
    parameters = parse_logfile_parameters(run_logfile)

    # Read the DB info
    DB = read_DB()
    
    # Define the file header
    HEADER = "sample;"       + \
             "runid;"        + \
             "seqplace;"     + \
             "mean;"         + \
             "median;"       + \
             "coverage;"     + \
             "N_regions;"    + \
             "mean_N_size;"  + \
             "N_count;"      + \
             "ambiguous_nt;" + \
             "SNPs;"         + \
             "insertions;"   + \
             "deletions;"    + \
             "lineage;"      + \
             "clades;"       + \
             "mutations;"    + \
             "Hospital;"     + \
             "Ct E;"         + \
             "Ct N;"         + \
             "Ct R;"         + \
             "Paired_end_reads;"  + \
             "Hospital_date;"     + \
             "P1/P2 ratio;"       + \
             "percent_sars-cov2;" + \
             "percent_homo-sapiens\n"

    # Open the file for writting
    outfile = open(args.outfile, "w")

    # Write header
    outfile.write(HEADER)

    # Write each line
    for cov in result_files:

        # Get the analysis file paths
        consensus_file = result_files[cov]['consensus']
        variants_file  = result_files[cov]['variants']
        wdepth_file    = result_files[cov]['wdepth']
        fastp_json     = result_files[cov]['fastp']
        adepth_file    = result_files[cov]['adepth']
        report_file    = result_files[cov]['report']

        # Get the number of reads from the fastp file
        nreads = parse_fastp(fastp_json)

        # Parse the consensus file
        ncount, ambcount, coverage, _, \
        nmean, _, number_nchunks = parse_consensus(consensus_file)
        

        # Parse the variants file
        inscount, delcount, snpcount, \
        clades, mutations = \
            parse_variants(variants_file, parameters["MINFREQ_CONS"])

        # Parse the wdepth file
        _, _, o_mean, _, o_median, _ = \
            parse_wdepth(wdepth_file, parameters["MIN_DEPTH"])
        
        # Parse the adepth file
        pool_ratio = parse_adepth(adepth_file)        

        # Parse the report file
        perc_sars, perc_homo = parse_report(report_file)

        # Check if COV exist in the DB (negative controls are not in DB)
        if cov in DB:
            pcr_e     = DB[cov]["pcr_e_c"]
            pcr_n     = DB[cov]["pcr_n_c"]
            pcr_r     = DB[cov]["pcr_r_c"]
            hosp_date = DB[cov]["date"]
            hospital  = DB[cov]["hospital"]
        else:
            pcr_e     = "NA"
            pcr_n     = "NA"
            pcr_r     = "NA"
            hosp_date = "NA"
            hospital  = "NA"
        
        # Get the lineage in a safe way
        lineage = 'None'
        if cov in lineages:
            lineage = lineages[cov]

        # Get the formatted row text
        record = "{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{}\n".format(
        cov,
        args.runid,
        seqplace,
        o_mean,
        o_median,
        coverage,
        number_nchunks,
        nmean,
        ncount,
        ambcount,
        snpcount,
        inscount,
        delcount,
        lineage,
        clades,
        mutations,
        hospital,
        pcr_e,
        pcr_n,
        pcr_r,
        nreads,
        hosp_date,
        pool_ratio,
        perc_sars,
        perc_homo
        )

        # Write the formatted row
        outfile.write(record)

    # Close the file
    outfile.close()

    return 0


#########################################################
# Main functions
#########################################################

def main():
    '''
    Main entry point
    '''
    args = parse_args()
    write_summary(args)

    return 0


if __name__ == '__main__':
    main()
