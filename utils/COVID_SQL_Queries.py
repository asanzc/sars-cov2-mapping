#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Utility Functions to perform SQL SELECT queries to the hscovid DB
'''

#---------------------------------------------------------------
__author__      = "Santiago Jiménez-Serrano"
__credits__     = ["Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import sys
import argparse
import COVID_Utils             as cutils
import COVID_SpainMap_Utils    as sp_map_utils
import COVID_loadDBCredentials as dbcredentials


##########################################################################
# Arguments parsing
##########################################################################

def parse_args():
    '''
    Parse arguments given to script
    '''

    parser = argparse.ArgumentParser(description='Performs SQL queries to the hscovid DB')
    parser.add_argument("-runid", "--runid", dest="runid", required=False, default='201113_M06710')
    args = parser.parse_args()

    return args



##########################################################################
# Basic functions
##########################################################################

def print_data(sql_data):
    '''
    Print to the console the specified table
    '''
    for record in sql_data:
        first_column = True
        for o in record:
            if first_column:
                print(o, end='')
                first_column = False
            else:
                print('\t', o, end='')
        print('')


def Execute_SELECT(sql_select):
    ''' 
    Connect to the DB, execute the SELECT 
    command and return the result-set
    '''

    # Gets a new connection to the DB
    cnx = dbcredentials.new_connection()

    # Create a cursor
    mycursor = cnx.cursor()

    # Run the SQL sentence
    mycursor.execute(sql_select)

    # Get all the SQL info
    SQL_info = mycursor.fetchall()

    # Close the cursor & the connection
    mycursor.close()
    cnx.close()

    # Return the result set
    return SQL_info



##########################################################################
# Execute SQL SELECT queries
##########################################################################

def Execute_SELECT_microreact_info():
    '''
    Return a result set with the Microreact
    information needed to perform a new submit
    '''
    return Execute_SELECT(get_SELECT_microreact_info())


def Execute_SELECT_RunInfo_ReportShort(runid):
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_RunInfo_ReportShort(runid))


def Execute_SELECT_RunInfo_ReportLong(runid):
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_RunInfo_ReportLong(runid))


def Execute_SELECT_NumSamplesInGISAID():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_NumSamplesInGISAID())


def Execute_SELECT_CountStatus():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_CountStatus())


def Execute_SELECT_NumSamplesByHospital_Short():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_NumSamplesByHospital_Short())


def Execute_SELECT_NumSamplesByHospital_Long():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_NumSamplesByHospital_Long())



##########################################################################
# Get SQL SELECT queries
##########################################################################

def get_SELECT_microreact_info():
    '''
    Return the SQL query with the Microreact
    information needed to perform a new submit
    '''

    return \
        " SELECT " \
	    "    CONCAT('COV', LPAD(s.gId, 6, '0')) AS ID, " + \
        "    s.seq_place, "                              + \
        "    s.run_code, "                               + \
	    "    s.coverage, "                               + \
	    "    s.median_depth, "                           + \
	    "    s.lineage AS lineage__autocolour, "         + \
	    "    h.name, "                                   + \
	    "    h.latitude AS hosp_latitude, "              + \
	    "    h.longitude AS hosp_longitude, "            + \
	    "    s.hospital_date, "                          + \
	    "    s.gender AS Gender__Autocolor, "            + \
	    "    s.age AS Age, "                             + \
	    "    s.residence_city, "                         + \
        "    s.residence_province, "                     + \
        "    s.sample_latitude, "                        + \
        "    s.sample_longitude, "                       + \
	    "    h.address "                                 + \
        " FROM sample s INNER JOIN hospital h  "         + \
        " ON s.hospital_id = h.id "                      + \
        " WHERE  "                                       + \
	    "    s.status LIKE 'Sequenced' "                 + \
        " ORDER BY " \
        "    s.id "


def get_SELECT_RunInfo_ReportShort(runid):
    '''
    <>
    '''

    return \
        "SELECT "                        + \
	    " s.run_code AS RUN_ID, "        + \
	    " s.status   AS STATUS,  "       + \
	    " COUNT(*)   AS NUM_SAMPLES "    + \
        "FROM sample s "                 + \
        "WHERE  "                        + \
	    " s.deleted_dt LIKE ''   AND "   + \
	    " s.run_code LIKE \"" + str(runid) + "\" " + \
        " GROUP BY s.run_code, s.status "  + \
        " ORDER BY s.run_code, s.status"


def get_SELECT_RunInfo_ReportLong(runid):
    '''
    <>
    '''

    return \
        "SELECT "                        + \
	    " s.run_code AS RUN_ID, "        + \
	    " s.status   AS STATUS,  "       + \
	    " h.name     AS HOSPITAL_NAME, " + \
	    " COUNT(*)   AS NUM_SAMPLES "    + \
        "FROM sample s, hospital h "     + \
        "WHERE  "                        + \
	    " s.hospital_id = h.id   AND "   + \
	    " s.deleted_dt LIKE ''   AND "   + \
	    " s.run_code LIKE \"" + str(runid) + "\" " + \
        " GROUP BY s.run_code, s.status, h.name "  + \
        " ORDER BY s.run_code, h.name, s.status"


def get_SELECT_NumSamplesInGISAID():
    '''
    <>
    '''
    
    return \
    "SELECT COUNT(*) "             + \
    "FROM sample s "               + \
    "WHERE s.gisaid NOT LIKE '' "


def get_SELECT_CountStatus():
    '''
    <>
    '''

    return \
        " SELECT s.status, COUNT(s.status) AS num " + \
        " FROM sample s"                            + \
        " WHERE s.deleted_dt LIKE ''"               + \
        " GROUP BY s.status " 


def get_SELECT_NumSamplesByHospital_Short():
    '''
    <>
    '''
    return \
        " SELECT "                    + \
        "    h.name, "                + \
        "    h.hosp_code, "           + \
        "    COUNT(*) AS NumSamples " + \
        " FROM sample s "             + \
        " LEFT JOIN hospital h ON (h.id = s.hospital_id AND h.deleted_dt = '') " + \
        " WHERE	    s.deleted_dt = '' " + \
        " GROUP BY	h.name "            + \
        " ORDER BY	h.hosp_code "


def get_SELECT_NumSamplesByHospital_Long():
    '''
    <>
    '''
    return \
        " SELECT "          + \
        "    h.name, "      + \
        "    h.hosp_code, " + \
        "    s.status, "    + \
        "    COUNT(*) "     + \
        " FROM sample s "   + \
        " LEFT JOIN hospital h ON (h.id = s.hospital_id AND h.deleted_dt = '') " + \
        " WHERE	    s.deleted_dt = '' " + \
        " GROUP BY	h.name, s.status "  + \
        " ORDER BY	h.hosp_code, s.status "



##########################################################################
# Main
##########################################################################

def main():
    
    args = parse_args()
    runid = args.runid
    
    print_data(Execute_SELECT_RunInfo_ReportShort(runid))
    print_data(Execute_SELECT_RunInfo_ReportLong(runid))

    print_data(Execute_SELECT_NumSamplesInGISAID())
    print_data(Execute_SELECT_CountStatus())
    print_data(Execute_SELECT_NumSamplesByHospital_Short())
    print_data(Execute_SELECT_NumSamplesByHospital_Long())

    #print_data(Execute_SELECT_microreact_info())

    return 0


if __name__ == '__main__':
    main()