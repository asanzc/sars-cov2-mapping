#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
SeqCovid-Spain Pipeline utilities
'''
#---------------------------------------------------------------
__author__      = "Galo A. Goig, Santiago Jiménez-Serrano"
__credits__     = ["Galo A. Goig", "Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import os
import sys
import COVID_SpainMap_Utils as sp_map_utils


def getEPIWeek(year, month, day, unknown_value = "unknown"):
    ''' 
    Gets the EPI week for the given date
    '''
    
    import math
    from datetime import date
    
    first_epi_week = date(2019, 12, 23)

    try:
        year      = int(year)
        month     = int(month)
        day       = int(day)
        hosp_date = date(year, month, day)
        epi_week  = math.floor(abs((hosp_date - first_epi_week).days) / 7)
    except ValueError:
        epi_week = unknown_value

    return epi_week


def getDetailedDate(date_value, unknown_value = "unknown"):
    '''
    Gets the detailed fields for a given date
    '''

    # Detailed Sample date values
    if date_value:
        dt = date_value.strftime('%Y-%m-%d')
        yy = date_value.strftime('%Y')
        mm = date_value.strftime('%m')
        dd = date_value.strftime('%d')
        ew = getEPIWeek(yy, mm, dd, unknown_value)
    else:
        dt = unknown_value
        yy = unknown_value
        mm = unknown_value
        dd = unknown_value
        ew = unknown_value

    return dt, yy, mm, dd, ew


def getInfoFromHospitalAddress(address, cov = 'null', unknown_value = 'UNKNOWN'):
    '''
    Get, in a safe way, the 
    '''
    
    if not address:
        prov = unknown_value
        city = unknown_value
        ccaa_name = unknown_value
        ccaa_code = unknown_value
        WARNING = "W[{}]: => Origin Address NOT EXIST in DB. User MUST set one. Set now hospital City & Province ({}, {}) \n".format(cov, prov, city)
        sys.stderr.write(WARNING)
    else:
        tokens = address.split(",")
        prov   = tokens[-2].strip().replace(" ", "_")
        city   = tokens[-3].strip().replace(" ", "_")    
        ccaa_name = sp_map_utils.get_CCAA(prov)
        ccaa_code = sp_map_utils.get_CCAA_ISO3166(prov)

    return prov, city, ccaa_name, ccaa_code


def getSafeGender(gender, unknown_value = 'unknown'):
    '''
    Gender safe parsing
    '''

    if gender == "M":
        return "Male"
    if gender == "F":
        return "Female"
    if gender == "Male" or gender == "Female":
        return gender
    return unknown_value  


def getSafeAge(age, unknown_value = 'unknown'):
    '''
    Age safe parsing
    '''

    if not age:
        return unknown_value

    try:
        return int(age)
    except ValueError:
        return unknown_value


def getSafeRoundedFloat(number, unknown_value = 'NA'):
    '''
    Gets, in a safe way, the specified number rounded to 2 digits
    '''    
    if not number:
        return unknown_value
    return round(number, 2)


def getSafeValue(value, unknown_value = 'NA'):
    '''
    Gets, in a safe way, the specified value
    '''    
    if not value:
        return unknown_value
    return value


def getSafeDate(date_value, unknown_value = 'NA'):
    '''
    Gets, in a safe way, a formatted date string in format yyyy-MM-dd
    '''
    if not date_value:
        return unknown_value
    return date_value.strftime('%Y-%m-%d')


def getSafeRCity(city, hospital_city, cov = 'null', silent = False):
    '''
    Gets, in a safe way, the Residence city for a given COV sample
    '''
    # If Residence City is unknown, set the hospital ones plus a '_h' suffix
    if not city:
        city = "{}_h".format(hospital_city)

        # Print Warning ?
        if not silent:
            WARNING = "W[{}]: => Origin City NOT EXIST. Using the hospital city ({}) \n".format(cov, city)
            sys.stderr.write(WARNING)

    return city
    

def getSafeRProvince(province, hospital_province, cov = 'null', silent = False):
    '''
    Gets, in a safe way, the Residence city for a given COV sample
    '''
    # If Residence Province is unknown, set the hospital ones plus a '_h' suffix
    if not province:
        province = "{}_h".format(hospital_province)

        if not silent:
            WARNING = "W[{}]: => Origin Province NOT EXIST. Using the hospital province ({}) \n".format(cov, province)
            sys.stderr.write(WARNING)

    return province


def getSafeSampleCoords(slat, slon, hlat, hlon, cov = 'null', silent = False):
    '''
    Gets, in a safe way, the Sample coordinates, setting the hospital ones if they are null    
    '''
    # If Sample Coordinates are unknown, set the hospital ones
    if not slat or not slon:
        slat = hlat
        slon = hlon

        if not silent:
            WARNING = "W[{}]: => Sample coordinates NOT EXIST. Using the hospital ones! \n".format(cov)
            sys.stderr.write(WARNING)
    
    return slat, slon


def get_shipcode(ship_number):
    '''
    Gets the Shipment code for a given number with the form COVID19-021-Exxxx
    '''

    if not ship_number:
        ship_number = 9999
    return "COVID19-021-E{:04d}".format(ship_number)


def gisaid_originlab_code(seq_place):
    '''
    Gets a specific code for the GISAID virus name according to the seq place
    '''
    if seq_place == "IBV":
        return "97"
    if seq_place == "FISABIO":
        return "98"
    return "99"


def number2cov(number):
    '''
    Gets the COV id given a int number
    '''

    cov = str(number)
    cov = "COV" + (6 - len(cov))*"0" + cov
    return cov


def cov2number(cov):
    '''
    Converts a COV code to the corresponding number (safe parsing). 
    Return -1 if some error happens
    '''

    # Preconditions
    if not cov:
        return -1
    
    # Convert to string
    cov = str(cov)

    # Remove the COV prefix
    if cov.startswith("COV"):
        cov = cov.replace("COV", "")

    # Remove the starting zeros
    while cov.startswith("0"):
        cov = cov[1:len(cov)]

    # Try parse    
    try:
        return int(cov)
    except ValueError:
        return -1
    except Exception:
        return -1


def str2bool(v):
    '''
    String to boolean utility for the args.parse functions
    '''
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        import argparse
        raise argparse.ArgumentTypeError('Boolean value expected.')


def getTreeBranch(lineage):
    ''' 
    Get the main lineage branch from the specified lineage
    '''
    lineage = lineage.split(".")
    branch = lineage[0]
    return (branch)


def getSeqPlaceFolder(seqplace):
    '''
    Gets the real Folder name for a SeqPlace DB item
    '''
    if seqplace == "FISABIO":
        return "seqfisabio"
    return seqplace


def getSeqPlaceFromPath(run_dir):
    '''
    Get the Seqcuencing Place according the run folder path
    '''

    # Path is the absolute and normalized PATH for the output directory
    rundir = os.path.abspath(run_dir)

    # Check path exists
    if not os.path.exists(rundir):
        W = "Run directory does not exist: {}\n".format(rundir)
        sys.stderr.write(W)
        return 'UNKNOWN'

    # Tokenize the path
    tokens = rundir.split('/')

    # Get the corresponding path token for the seq place
    if len(tokens) < 3:
        return 'UNKNOWN'

    # Get the corresponding token
    seq_place = tokens[-3]
    
    # FISABIO special folder
    if seq_place == "seqfisabio":
        return 'FISABIO'
    
    return seq_place
    

def getAnalysisFolder(seqplace, runid, seq_tech = "illumina"):
    '''
    Gets the Analysis Folder for a given seqplace and runid
    '''
    if not seqplace or not runid:
        return ""

    # Check sequencing technology
    if "iontorrent" in seq_tech.lower():
        seq_tech = "IonTorrent"
    else:
        seq_tech = "illumina"


    f = getSeqPlaceFolder(seqplace)
    base_folder = "/data2/COVID19/analysis/"
    return base_folder + f + "/" + seq_tech + "/" + runid


def getSequencingDataFolder(seqplace, runid, seq_tech = "illumina"):
    '''
    Gets the Analysis Folder for a given seqplace and runid
    '''
    if not seqplace or not runid:
        return ""

    # Check sequencing technology
    if "iontorrent" in seq_tech.lower():
        seq_tech = "IonTorrent"
    else:
        seq_tech = "illumina"

    f = getSeqPlaceFolder(seqplace)
    base_folder = "/data2/COVID19/sequencing_data/"
    return base_folder + f + "/" + seq_tech + "/" + runid


def getSQLTableStatus(SQL_info):
    '''
    Gets a status for the specified sql table
    '''
    if SQL_info is None:
        return "ERROR"        
    elif len(SQL_info) == 0:
        return "NODATA"        
    # The record contains some data
    return "DATA"

def print_cov_record_info(COV_info):
    '''
    Print to the console the specified COV_info record (dictionary)
    '''

    # Get the maximun key length
    max_length = 0
    for key in COV_info:
        n = len(key)
        if n > max_length:
            max_length = n

    # Print the values with format [key : value], padding the key with spaces
    str_format = "{:" + str(max_length) + "}:{}"
    for key in COV_info:        
        print(str_format.format(key, COV_info[key]))
    print()
